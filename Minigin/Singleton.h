#pragma once

template <class T>
class Singleton
{
public:
	static T* GetInstance()
	{
		assert(m_IsCreated);
		return m_pInstance;
	}
	static void CreateInstance()
	{
		m_pInstance = new T();
		m_IsCreated = true;
	}
	static void DeleteInstance()
	{
		delete m_pInstance;
		m_pInstance = nullptr;
		m_IsCreated = false;
	}

	virtual ~Singleton() = default;

private:
	static bool m_IsCreated;
	static T * m_pInstance;

	explicit Singleton() = default;

	Singleton(const Singleton&) = delete;
	Singleton(Singleton&&) = delete;
	Singleton& operator=(const Singleton&) = delete;
	Singleton& operator=(Singleton&&) = delete;
};

template <class T>
bool Singleton<T>::m_IsCreated = false;

template <class T>
T* Singleton<T>::m_pInstance = nullptr;