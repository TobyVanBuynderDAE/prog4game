#pragma once
#include "BaseComponent.h"

class TransformComponent final : public BaseComponent
{
public:
	explicit TransformComponent();
	virtual ~TransformComponent() = default;

	void Update(float deltaTime) override;
	void Translate(const glm::vec3& trs);

	const glm::vec3& GetPosition() const;
	void SetPosition(const glm::vec3& pos);

private:
	glm::vec3 m_Position;

	TransformComponent & operator=(const TransformComponent& b) = delete;
	TransformComponent & operator=(TransformComponent&& b) = delete;
	TransformComponent(const TransformComponent& b) = delete;
	TransformComponent(TransformComponent&& b) = delete;
};
