#include "TransformComponent.h"

TransformComponent::TransformComponent()
	: m_Position(0,0,0)
{
}

void TransformComponent::Update(float deltaTime)
{
	UNREFERENCED_PARAMETER(deltaTime);
}
void TransformComponent::Translate(const glm::vec3& trs)
{
	m_Position += trs;
}

const glm::vec3& TransformComponent::GetPosition() const
{
	return m_Position;
}
void TransformComponent::SetPosition(const glm::vec3& pos)
{
	m_Position = pos;
}