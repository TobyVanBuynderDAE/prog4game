#include "Game.h"

int main(int argc, char* argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	Game* pGame{ new Game() };
	pGame->Run();
	delete pGame;

	return 0;
}