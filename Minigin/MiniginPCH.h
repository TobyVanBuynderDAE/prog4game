#pragma once

// Target version
#include "targetver.h"

// General includes
#include <stdio.h>
#include <iostream> // std::cout
#include <sstream> // stringstream
#include <tchar.h>
#include <memory> // smart pointers
#include <vector>
#include <chrono>
#include <thread> // threading

// Windows
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// Gamepad support
#include <Xinput.h>
#pragma comment(lib,"xinput.lib") 

// Tools
#pragma warning(push)
#pragma warning (disable:4201)
#include "glm.hpp" // OpenGL math library
#pragma warning(pop)
#include "vld.h" // Memory leaks

// SDL framework
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "SDL.h"

// Own includes
#include "Log.h" // Various logging functions