#pragma once

class GameObject;

class GameScene
{
public:
	explicit GameScene() = default;
	virtual ~GameScene() = default;

	virtual void Update(float deltaTime) = 0;
	virtual void LateUpdate(float deltaTime) = 0;

	void AddObject(GameObject* pObj);

private:
	std::vector<GameObject*> m_pObjects;

	GameScene & operator=(const GameScene& g) = delete;
	GameScene & operator=(GameScene&& g) = delete;
	GameScene(const GameScene& g) = delete;
	GameScene(GameScene&& g) = delete;
};