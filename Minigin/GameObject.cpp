#include "GameObject.h"
#include "BaseComponent.h"
#include "TransformComponent.h"

GameObject::GameObject()
	: m_pTransform(new TransformComponent())
{
}

GameObject::~GameObject()
{
	for(BaseComponent* pComp : m_pComponents)
		if (pComp) delete pComp;
}

void GameObject::Update(float deltaTime)
{
	UNREFERENCED_PARAMETER(deltaTime);
}
void GameObject::LateUpdate(float deltaTime)
{
	UNREFERENCED_PARAMETER(deltaTime);
}

void GameObject::AddComponent(BaseComponent* pComp)
{
	m_pComponents.push_back(pComp);
}

TransformComponent* GameObject::GetTransform() const
{
	return m_pTransform;
}