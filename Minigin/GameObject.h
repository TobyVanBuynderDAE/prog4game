#pragma once

class TransformComponent;
class BaseComponent;

class GameObject
{
public:
	explicit GameObject();
	virtual ~GameObject();

	void Update(float deltaTime);
	void LateUpdate(float deltaTime);

	void AddComponent(BaseComponent* pComp);
	TransformComponent* GetTransform() const;

	template <class T>
	T* GetComponent() const;

private:
	TransformComponent* m_pTransform;
	std::vector<BaseComponent*> m_pComponents;

	GameObject & operator=(const GameObject& g) = delete;
	GameObject & operator=(GameObject&& g) = delete;
	GameObject(const GameObject& g) = delete;
	GameObject(GameObject&& g) = delete;
};

template <class T>
T* GameObject::GetComponent() const
{
	const type_info& ti = typeid(T);
	for(BaseComponent* pComp : m_pComponents)
	{
		if(pComp && typeid(pComp) == ti)
		{
			return static_cast<T*>(pComp);
		}
	}
	return nullptr;
}