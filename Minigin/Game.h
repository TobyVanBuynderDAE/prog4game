#pragma once

class Game
{
public:
	explicit Game();
	~Game();
	void Run();

private:
	

	bool ProcessInput() const;
	void Update(float deltaTime);
	void LateUpdate(float deltaTime);
	void Render() const;
	void Init();
	void Cleanup();

	Game & operator=(const Game& g) = delete;
	Game & operator=(Game&& g) = delete;
	Game(const Game& g) = delete;
	Game(Game&& g) = delete;
};