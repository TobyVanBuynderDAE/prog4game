#pragma once
#include "Singleton.h"

class RenderComponent;

class Renderer final : public Singleton<Renderer>
{
public:
	friend class Singleton<Renderer>;

	virtual ~Renderer() = default;

	void Render() const
	{
		
	}

private:


	explicit Renderer();

	Renderer(const Renderer&) = delete;
	Renderer(Renderer&&) = delete;
	Renderer& operator=(const Renderer&) = delete;
	Renderer& operator=(Renderer&&) = delete;
};
