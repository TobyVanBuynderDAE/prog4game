#pragma once

class BaseComponent
{
public:
	explicit BaseComponent() = default;
	virtual ~BaseComponent() = default;

	virtual void Update(float deltaTime) = 0;

private:
	BaseComponent & operator=(const BaseComponent& b) = delete;
	BaseComponent & operator=(BaseComponent&& b) = delete;
	BaseComponent(const BaseComponent& b) = delete;
	BaseComponent(BaseComponent&& b) = delete;
};