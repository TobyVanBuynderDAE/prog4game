#include "Game.h"
#include "Renderer.h"

Game::Game()
{
	Init();
}
Game::~Game()
{
	Cleanup();
}

void Game::Run()
{
	auto lastTick = std::chrono::high_resolution_clock::now();
	while (true)
	{
		const auto newTick = std::chrono::high_resolution_clock::now();
		const float deltaTime{ float(std::chrono::duration_cast<std::chrono::milliseconds>(newTick - lastTick).count()) };

		if (ProcessInput())
			break;
		Update(deltaTime);
		LateUpdate(deltaTime);
		Render();

		lastTick = newTick;
	}
}

bool Game::ProcessInput() const
{
	return false;
}
void Game::Update(float deltaTime)
{
	UNREFERENCED_PARAMETER(deltaTime);

	std::cout << "Update Running" << std::endl;
}
void Game::LateUpdate(float deltaTime)
{
	UNREFERENCED_PARAMETER(deltaTime);

	std::cout << "LateUpdate Running" << std::endl;
}
void Game::Render() const
{
	std::cout << "Render Running" << std::endl;
	Renderer::GetInstance()->Render();
}
void Game::Init()
{
	Renderer::CreateInstance();
}
void Game::Cleanup()
{
	Renderer::DeleteInstance();
}